﻿using System.ComponentModel.DataAnnotations;

namespace EF_example.Models
{
    internal class AddressClient : Base
    {
        [Required]
        public Client? Client { get; set; }
        public int ClientID { get; set; }
        public string? Country { get; set; }
        public string? City { get; set; }
        public string? Street { get; set; }
        public string? House { get; set; }
        public int Apartmen { get; set; }

        public override string ToString()
        {
            return $"{Country}, ***";
        }
    }
}
