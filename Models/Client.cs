﻿using System.ComponentModel.DataAnnotations;

namespace EF_example.Models
{
    internal class Client : Base
    {

        [Required]
        [MaxLength(100)]
        public string? FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        public string? LastName { get; set; }
        public AddressClient? Address { get; set; }
        public ICollection<Account>? Accounts { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }

    }
}
