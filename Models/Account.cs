﻿namespace EF_example.Models
{
    internal class Account : Base
    {
        public Client? Client { get; set; }
        public int ClientID { get; set; }
        public string? Currency { get; set; }
        public decimal Volume { get; set; }

        public override string ToString()
        {
            return $"{Currency}/{Volume}";
        }
    }
}
