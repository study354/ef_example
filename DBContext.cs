﻿using EF_example.Models;
using Microsoft.EntityFrameworkCore;

namespace EF_example
{

    internal class IDBContext : DbContext
    {
        private readonly string? _connectingString = Environment.GetEnvironmentVariable("ConnectingString");
        public DbSet<AddressClient> AddressClient { get; set; } = null!;
        public DbSet<Client> Client { get; set; } = null!;
        public DbSet<Account> Account { get; set; } = null!;

        public IDBContext()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder == null)
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }
            optionsBuilder.UseNpgsql(_connectingString);
        }


    }
}
