﻿using Bogus;
using EF_example.Models;
using Microsoft.EntityFrameworkCore;

namespace EF_example
{
    internal class ExampleWork : IDisposable
    {
        private IDBContext _dbContext;

        public ExampleWork(IDBContext dBContext)
        {
            _dbContext = dBContext;
        }


        public void CreateBase()
        {
            _dbContext.Database.EnsureCreated();
        }

        public void DeleteBase()
        {
            _dbContext.Database.EnsureDeleted();
        }


        public void SetTestData(int countRow = 20)
        {
            var faker = new Faker();
            for (int i = 0; i < countRow; i++)
            {
                _dbContext.Client.Add(new Client()
                {
                    FirstName = faker.Name.FirstName(),
                    LastName = faker.Name.LastName(),
                    Address = new AddressClient()
                    {
                        Country = faker.Address.Country(),
                        City = faker.Address.City(),
                        Street = faker.Address.StreetName(),
                        House = faker.Address.BuildingNumber(),
                        Apartmen = faker.Random.Int(1, 500)
                    },
                    Accounts = new List<Account>(){
                new Account() { Currency = faker.Finance.Currency().Code, Volume = faker.Finance.Amount()},
                new Account() { Currency = faker.Finance.Currency().Code, Volume = faker.Finance.Amount() }
                }
                });
            }

            _dbContext.SaveChanges();
        }

        public void ReadAllData()
        {
            var clients = _dbContext.Client.Include(cl => cl.Accounts).Include(cl => cl.Address);
            string top = $"| {"ID",-5} | {"FirstName",-15} | {"LastName",-15} | {"Address",-50} | {"Accounts",-30} |";
            string separator = new string('-', top.Length);
            Console.WriteLine($"{separator}\n{top}\n{separator}");
            foreach (Client client in clients)
            {
                Console.WriteLine($"| {client.Id,-5} | {client.FirstName,-15} | {client.LastName,-15} | {client.Address,-50} | {string.Join(" ", client.Accounts),-30} |");
            }
            Console.WriteLine(separator);
        }

        public Client AddClient(Client client)
        {
            var result = _dbContext.Client.Add(client);
            _dbContext.SaveChanges();
            return result.Entity;
        }

        public Account? AddAccount(Account? account, Client? client)
        {
            if (account == null || client == null) { return null; }
            account.Client = GetOrCreateClient(client);
            var result = _dbContext.Account.Add(account);
            _dbContext.SaveChanges();
            return result.Entity;
        }

        public AddressClient? AddAddressClient(AddressClient? address, Client? client)
        {
            if (address == null || client == null) { return null; }
            address.Client = GetOrCreateClient(client);
            var result = _dbContext.AddressClient.Add(address);
            _dbContext.SaveChanges();
            return result.Entity;
        }

        public Client GetOrCreateClient(Client inputClient)
        {
            var client = _dbContext.Client.FirstOrDefault(cl => cl.FirstName == inputClient.FirstName && cl.LastName == inputClient.LastName);
            if (client == null)
            {
                return AddClient(inputClient);
            }
            return client;
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
