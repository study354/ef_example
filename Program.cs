﻿using EF_example;
using EF_example.Models;


// Пересоздать базу
using (var example = new ExampleWork(new IDBContext()))
{
    example.DeleteBase();
    example.CreateBase();
}


// Заполнить базу
using (var example = new ExampleWork(new IDBContext()))
{
    example.SetTestData(countRow: 100);
}

// Ввести содержимое всех таблиц
using (var example = new ExampleWork(new IDBContext()))
{
    example.ReadAllData();
}


// Добавить данные в ручную
using (var example = new ExampleWork(new IDBContext()))
{
    Client? client = null;


    Console.WriteLine("In which table (Client / Account / AddressClient) do You need to enter the data?");
    string? nameTable = Console.ReadLine();
    switch (nameTable?.ToLower())
    {
        case "client":
            client = InputData.GetClient();
            if (client != null)
            {
                Console.WriteLine($"Result: {example.AddClient(client)}.");
            }
            break;
        case "account":
            Console.WriteLine($"Result: {example.AddAccount(InputData.GetAccount(), InputData.GetClient())}.");
            break;
        case "addressclient":
            Console.WriteLine($"Result: {example.AddAddressClient(InputData.GetAddress(), InputData.GetClient())}.");
            break;
        default:
            Console.WriteLine("in such a table does not exist");
            break;
    }
}