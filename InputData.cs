﻿using EF_example.Models;

namespace EF_example
{
    internal class InputData
    {
        public static Client? GetClient()
        {
            Console.WriteLine("Input FirstName:");
            string? firstName = Console.ReadLine();
            Console.WriteLine("Input LastName:");
            string? lastName = Console.ReadLine();
            if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
            {
                Console.WriteLine("Incorrect data Client entered");
                return null;
            }
            return new Client() { FirstName = firstName, LastName = lastName };
        }

        public static Account? GetAccount()
        {
            Console.WriteLine("Input Currency:");
            string? currency = Console.ReadLine();
            Console.WriteLine("Input Volume:");
            if (string.IsNullOrEmpty(currency) || !decimal.TryParse(Console.ReadLine(), out var volume))
            {
                Console.WriteLine("Incorrect data Account entered");
                return null;
            }
            return new Account() { Currency = currency, Volume = volume };
        }

        public static AddressClient? GetAddress()
        {
            Console.WriteLine("Input Country:");
            string? country = Console.ReadLine();
            Console.WriteLine("Input City:");
            string? city = Console.ReadLine();
            Console.WriteLine("Input Street:");
            string? street = Console.ReadLine();
            Console.WriteLine("Input House:");
            string? house = Console.ReadLine();

            Console.WriteLine("Input Apartmen:");
            if (new List<string?>() { country, city, street, house }.Any(field => string.IsNullOrEmpty(field)) || !int.TryParse(Console.ReadLine(), out var apartmen))
            {
                Console.WriteLine("Incorrect data Address entered");
                return null;
            }
            return new AddressClient() { Country = country, City = city, Street = street, House = house, Apartmen = apartmen };
        }

    }
}
